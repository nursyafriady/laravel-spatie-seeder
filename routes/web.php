<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
// use Spatie\Permission\Models\Role;

Route::get('/', function () {
    return view('welcome');
    // $user = auth()->user();
    // dd($user);
    // $role = Role::find(1);
    // dd($user);
    // $role->givePermissionTo('view post');
    // $role->givePermissionTo('add post', 'edit post', 'delete post', 'view post');
    // $user->syncPermissions(['add post', 'delete post']);
    // dd($user->hasAllPermissions(['add post', 'edit post', 'delete post', 'view post']));
    // dd($user->can(['view post']));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('role:admin')->group(function () {
    //taruh routing yang menggunakan admin
    Route::get('admin', 'AdminController@index')->name('admin');
});
