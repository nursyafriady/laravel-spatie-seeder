<?php

use Illuminate\Database\Seeder;
use App\User;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'zakwan',
            'email' => 'zakwan@gmail.com',
            'password' => bcrypt(12345678)
        ]);

        $user->assignRole('user');
    }
}
